import { Router } from "express";
const roleRoute = require('./role.route');
const userRoute = require('./user.route');
const authRoute = require('./auth.route')

import { UnauthorizedError } from "../utils/error";
import JWTProvider from "../utils/jwt-provider";
const db = require('../models');
const User = db.user;

const verify = async (req, res, next) => {
  try {
    const token = JWTProvider.verifyToken(req);
    if (token) {
      let user = await User.findOne({ where: { id: JWTProvider.getBsonSub(req) } });
      if (!user) return next(new UnauthorizedError("Unauthorized."));
      return next();
    }
  } catch (err) {
    console.error(err);
  }

  return next(new UnauthorizedError("Bad Credential."));
};

export default ({ config, db }) => {
  let api = Router();
  // api.use('/users', userRoute);
  // api.use('/roles', roleRoute);
  api.use('/auth', authRoute);
  api.use(verify);
  api.use('/roles', roleRoute);
  api.use('/users', userRoute);

  return api;
};
