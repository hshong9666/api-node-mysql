const role = require("./role");

module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("users", {
        username: {
            type: DataTypes.TEXT,
            unique: true,
            allowNull: false
        },
        password: {
            type: DataTypes.TEXT,
            unique: true,
            allowNull: false
        },
        email: {
            type: DataTypes.TEXT,
            unique: true,
            allowNull: false
        },
        profile: {
            type: DataTypes.TEXT,
        },
        is_active: {
            type: DataTypes.INTEGER,
            defaultValue: 1
        },
        role_id: {
            type: DataTypes.INTEGER,
            references: {
                model: role, // 'roles' would also work
                key: 'id'
            },
            allowNull: false
        },
        created_by_id: {
            type: DataTypes.INTEGER,
            // references: {
            //     model: User,
            //     key: 'id'
            // }
        },
        updated_by_id: {
            type: DataTypes.INTEGER,
            // references: {
            //     model: User,
            //     key: 'id'
            // },
            defaultValue: null
        },
        is_deleted: {
            type: DataTypes.DATE,
            defaultValue: null
        },
        token: {
            type: DataTypes.TEXT,
        }
    });

    return User
}