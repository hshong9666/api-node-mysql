// const role = require("./role");
const db = require('../models')
const user = require("./user");

const Role = db.role;

module.exports = (sequelize, DataTypes) => {
    const Permission = sequelize.define("permission", {
        name: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        parent_id: {
            type: DataTypes.INTEGER,
            // references: {
            //     model: role,
            //     key: 'id'
            // },
            // allowNull: false
        },
        role_id: {
            type: DataTypes.INTEGER,
            // references: {
            //     model: Role,
            //     key: 'role_id'
            // },
            // allowNull: false
        },
        is_active: {
            type: DataTypes.INTEGER,
            defaultValue: 1
            
        },
        is_deleted: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_created: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_view: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_update: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_delete: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_cancel: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_block_schedule: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_accept: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_approve: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_settle: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_send_notification: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_print: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_export_excel: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_access: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_view_report: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_operation: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_view_user_location: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_assign_driver: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        is_view_driver_status: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        
        created_by_id: {
            type: DataTypes.INTEGER,
            references: {
                model: user,
                key: 'id'
            },
            allowNull: false
        },
        updated_by_id: {
            type: DataTypes.INTEGER,
            references: {
                model: user,
                key: 'id'
            },
            defaultValue: null
        },
    });

    // Permission.hasOne(Role, {as: 'Role', foreignKey: 'role_id'});

    return Permission
}