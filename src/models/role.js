const user = require("./user");

module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define("roles", {
        role_name: {
            type: DataTypes.TEXT,
            unique: true,
            allowNull: false
        },
        description: {
            type: DataTypes.TEXT,
        },
        type: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        is_active: {
            type: DataTypes.INTEGER,
            defaultValue: 1
            
        },
        created_by_id: {
            type: DataTypes.INTEGER,
            references: {
                model: user,
                key: 'id'
            },
            allowNull: false
        },
        updated_by_id: {
            type: DataTypes.INTEGER,
            references: {
                model: user,
                key: 'id'
            },
            defaultValue: null
        },
        is_deleted: {
            type: DataTypes.DATE,
            defaultValue: null
        },
    });

    return Role
}